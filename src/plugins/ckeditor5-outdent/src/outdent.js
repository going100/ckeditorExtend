/**
 * @license Copyright (c) 2003-2018, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md.
 */

/**
 * @module alignment/alignment
 */

import Plugin from '@ckeditor/ckeditor5-core/src/plugin';

import OutdentEditing from './outdentediting';
import OutdentUi from './outdentui';


export default class Outdent extends Plugin {
	/**
	 * @inheritDoc
	 */
	static get requires() {
		return [ OutdentEditing, OutdentUi ];
	}

	/**
	 * @inheritDoc
	 */
	static get pluginName() {
		return 'Outdent';
	}
}
