/**
 * @license Copyright (c) 2003-2018, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md.
 */

/**
 * @module alignment/alignmentediting
 */
import Plugin from '@ckeditor/ckeditor5-core/src/plugin';
import Outdentcommand from './outdentcommand';

const OUTDENT = 'outdent';

/**
 * The alignment editing feature. It introduces the {@link module:alignment/alignmentcommand~AlignmentCommand command} and adds
 * the `alignment` attribute for block elements in the {@link module:engine/model/model~Model model}.
 * @extends module:core/plugin~Plugin
 */
export default class OutdentEditing extends Plugin {
	/**
	 * @inheritDoc
	 */
	init() {
		const editor = this.editor;
		// Allow bold attribute on text nodes.
		editor.model.schema.extend( '$text', { allowAttributes: OUTDENT } );


		// Create bold command.
		editor.commands.add( OUTDENT, new Outdentcommand(editor) );

		// Set the Ctrl+B keystroke.
		editor.keystrokes.set( 'CTRL+O', OUTDENT );
	}
}

/**
 * 将20的倍数帅选出来
 * @returns {Array}
 * @private
 */
function _getValuesAry(){
	var valueArys=[];
	for(var num=1;num<=780;num++){
		if(num%20===0){
			valueArys.push(num+"");
		}
	}
	return valueArys;
}

function _getViewConfigs(valueArys) {
	var backJson={};
	valueArys.forEach(function (item, index) {
		backJson[item+""]={
			name: 'p',
			key: 'style',
			value: {
				'margin-left': item+'px',
			}
		}
	})
	return backJson;
}
