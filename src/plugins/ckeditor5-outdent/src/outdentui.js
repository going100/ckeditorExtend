/**
 * @license Copyright (c) 2003-2018, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md.
 */

/**
 * @module alignment/alignmentui
 */

import Plugin from '@ckeditor/ckeditor5-core/src/plugin';
import ButtonView from '@ckeditor/ckeditor5-ui/src/button/buttonview';

import OutdentIcon from '../theme/icons/outdent.svg';

/**
 * The default alignment UI plugin.
 *
 * It introduces the `'alignment:left'`, `'alignment:right'`, `'alignment:center'` and `'alignment:justify'` buttons
 * and the `'alignment'` dropdown.
 *
 * @extends module:core/plugin~Plugin
 */

const OUTDENT = 'outdent';

export default class IndentUI extends Plugin {
	/**
	 * @inheritDoc
	 */
	init() {
		const editor = this.editor;
		const t = editor.t;

		// Add bold button to feature components.
		editor.ui.componentFactory.add( OUTDENT, locale => {
			const command = editor.commands.get( OUTDENT );
			const view = new ButtonView( locale );

			view.set( {
				label: t( 'Decrease Indent' ),
				icon: OutdentIcon,
				keystroke: 'CTRL+I',
				tooltip: true
			} );

			view.bind( 'isEnabled' ).to( command );
			view.bind( 'isOn' ).to( command, 'value', value =>{return false;});
			// Execute command.
			this.listenTo( view, 'execute', ()=>{
				editor.execute( OUTDENT, { value: "20px" } );
				editor.editing.view.focus();
			} );

			return view;
		} );
	}
}

// Sets the alignment attribute on blocks.
// @private
function setIndentOnSelection( blocks, writer, alignment ) {
	for ( const block of blocks ) {
		writer.setAttribute( "margin", alignment, block );
	}
}

