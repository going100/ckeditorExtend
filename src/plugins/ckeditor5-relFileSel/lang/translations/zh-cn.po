# Copyright (c) 2003-2018, CKSource - Frederico Knabben. All rights reserved.
msgid ""
msgstr ""
"Language-Team: Chinese (China) (https://www.transifex.com/ckeditor/teams/11143/zh_CN/)\n"
"Language: zh_CN\n"
"Plural-Forms: nplurals=1; plural=0;\n"


msgctxt "Label for the insert image toolbar button."
msgid "Insert rel file"
msgstr "插入关联"

msgctxt "Toolbar button tooltip for making the text justified."
msgid "Upload attachement"
msgstr "上传附件"

msgctxt "Toolbar button tooltip for making the text justified."
msgid "Associate document"
msgstr "关联文档"

msgctxt "Toolbar button tooltip for making the text justified."
msgid "set up"
msgstr "设置"

msgctxt "Toolbar button tooltip for making the text justified."
msgid "Link type"
msgstr "关联类型"

msgctxt "Toolbar button tooltip for making the text justified."
msgid "Link"
msgstr "关联链接"


