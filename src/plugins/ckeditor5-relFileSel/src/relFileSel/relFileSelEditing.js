/**
 * @license Copyright (c) 2003-2018, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md.
 */

/**
 * @module relFileSel/relFileSelEditing.js
 */

import Plugin from '@ckeditor/ckeditor5-core/src/plugin';
import RelFileSelCommand from './relFileSelCommand';

/**
 * Image upload editing plugin.
 *
 * @extends module:core/plugin~Plugin
 */
export default class RelFileSelEditing extends Plugin {

	/**
	 * @inheritDoc
	 */
	init() {
		const editor = this.editor;
		editor.model.schema.extend( '$text', { allowAttributes: 'linkHref' } );
		// Create bold command.
		editor.commands.add( 'insertRelFile', new RelFileSelCommand(editor) );
	}

}


