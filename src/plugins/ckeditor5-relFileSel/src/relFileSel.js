/**
 * @license Copyright (c) 2003-2018, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md.
 */

import Plugin from '@ckeditor/ckeditor5-core/src/plugin';
import RelFileSelUI from './relFileSel/relFileSelUI';
import RelFileSelEditing from './relFileSel/relFileSelEditing';

export default class RelFileSel extends Plugin {
	/**
	 * 定义插件名称
	 */
	static get pluginName() {
		return 'RelFileSel';
	}

	/**
	 * 实现插件的关键接口
	 */
	static get requires() {
		return [RelFileSelEditing,RelFileSelUI];
	}
}
