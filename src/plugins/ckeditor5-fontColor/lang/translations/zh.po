# Copyright (c) 2003-2018, CKSource - Frederico Knabben. All rights reserved.
msgid ""
msgstr ""
"Language-Team: Chinese (China) (https://www.transifex.com/ckeditor/teams/11143/zh_CN/)\n"
"Language: zh_CN\n"
"Plural-Forms: nplurals=1; plural=0;\n"

msgctxt "Toolbar button tooltip for aligning the text to the left."
msgid "Text Color"
msgstr "字體顏色"
