
import Plugin from '@ckeditor/ckeditor5-core/src/plugin';

import FontColoriting from './fontColoriting';
import FontColorUi from './fontColorUi';

/**
 * 字体颜色
 */
export default class FontColor extends Plugin {
	/**
	 * @inheritDoc
	 */
	static get requires() {
		return [ FontColoriting, FontColorUi ];
	}

	/**
	 * @inheritDoc
	 */
	static get pluginName() {
		return 'fontColor';
	}
}
