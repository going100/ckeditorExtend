/**
 * @license Copyright (c) 2003-2018, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md.
 */

/**
 * @module font/fontsize/utils
 */

export function normalizeOptions( configuredOptions ) {
	// Convert options to objects.
	return configuredOptions
		.map( generatePixelPreset )
		// Filter out undefined values that `getOptionDefinition` might return.
		.filter( option => !!option );
}


// Creates a predefined preset for pixel size.
//
// @param {Number} size Font size in pixels.
// @returns {module:font/fontsize~FontSizeOption}
function generatePixelPreset( size ) {
	const sizeName = String( size );

	return {
		title: sizeName,
		model: size,
		view: {
			name: 'span',
			styles: {
				'color': `#${ size }`
			},
			priority: 5
		}
	};
}
