/**
 * @license Copyright (c) 2003-2018, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md.
 */

/**
 * @module heading/headingui
 */

import Plugin from '@ckeditor/ckeditor5-core/src/plugin';
import ButtonView from '@ckeditor/ckeditor5-ui/src/button/buttonview';
import './../theme/fontColor.css';
import './../theme/colpick.css';
import fontColorIcon from '../theme/icons/fontColor.svg';
import {buildDefinition} from "../../ckeditor5-font/src/utils";
import {normalizeOptions} from "./utils";

const FONT_COLOR = 'fontColor';

export default class FontColorUi extends Plugin {
	/**
	 * @inheritDoc
	 */
	init() {
		const editor = this.editor;
		const t = editor.t;

		// Setup `imageUpload` button.
		editor.ui.componentFactory.add( 'fontColor', locale => {
			const button = new ButtonView( locale );

			button.isEnabled = true;
			button.label = t( 'Text Color' );
			button.icon = fontColorIcon;
			button.class="fontColorBtn";
			button.id="fontColorBtn";
			button.tooltip = true;
			return button;
		});

		setTimeout(function () {
			$('.fontColorBtn').colpick({
				onSubmit:function(hsb,hex,rgb,el) {
					const options = normalizeOptions( [hex] ).filter( item => item.model );
					const definition = buildDefinition( FONT_COLOR, options );
					// Set-up the two-way conversion.
					editor.conversion.attributeToElement( definition );

					$(el).css('background-color', '#'+hex);
					$(el).colpickHide();
					editor.execute( 'fontColor',{value:hex});
					editor.editing.view.focus();
				}
			});
		},10)

	}
}

