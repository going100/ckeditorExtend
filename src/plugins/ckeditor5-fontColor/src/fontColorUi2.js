/**
 * @license Copyright (c) 2003-2018, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md.
 */

/**
 * @module heading/headingui
 */

import Plugin from '@ckeditor/ckeditor5-core/src/plugin';
import ButtonView from '@ckeditor/ckeditor5-ui/src/button/buttonview';
import './../theme/fontColor.css';
import './../theme/colpick.css';

import markerIcon from './../theme/icons/marker.svg';
import penIcon from './../theme/icons/pen.svg';
import eraserIcon from './../theme/icons/eraser.svg';

import ToolbarSeparatorView from '@ckeditor/ckeditor5-ui/src/toolbar/toolbarseparatorview';
import SplitButtonView from '@ckeditor/ckeditor5-ui/src/dropdown/button/splitbuttonview';
import { createDropdown, addToolbarToDropdown } from '@ckeditor/ckeditor5-ui/src/dropdown/utils';


function __getUiOptions(editor) {
	var allAry=[];
	for(var colNum=1;colNum<=5;colNum++){
		var ary=editor.config.get( 'fontColor.col'+colNum);
		allAry.push(...ary);
	}
	return allAry;
}

const FONT_COLOR = 'fontColor';

function __addRowBtnByCol(options,componentFactory, dropdownView,editorObj) {
	var buttons= options.map( option => {
		// Get existing fontColorer button.
		const buttonView = componentFactory.create( 'fontColor:' + option.model );

		// Update lastExecutedHighlight on execute.
		editorObj.listenTo( buttonView, 'execute', () => dropdownView.buttonView.set( { lastExecuted: option.model } ) );

		return buttonView;
	} );

	addToolbarToDropdown( dropdownView, buttons );

}

function __addRowBtnS(options, componentFactory, dropdownView,editorObj) {
// Create buttons array.
	const buttons = options.map(option => {
		// Get existing fontColorer button.
		const buttonView = componentFactory.create('fontColor:' + option.model);

		// Update lastExecutedHighlight on execute.
		editorObj.listenTo(buttonView, 'execute', () => dropdownView.buttonView.set({lastExecuted: option.model}));

		return buttonView;
	});

	// Make toolbar button enabled when any button in dropdown is enabled before adding separator and eraser.
	dropdownView.bind('isEnabled').toMany(buttons, 'isEnabled', (...areEnabled) => areEnabled.some(isEnabled => isEnabled));
	addToolbarToDropdown(dropdownView, buttons);
}

export default class FontColorUi extends Plugin {
	/**
	 * Returns the localized option titles provided by the plugin.
	 *
	 * The following localized titles corresponding with default
	 * {@link module:highlight/highlight~HighlightConfig#options} are available:
	 *
	 * * `'Yellow marker'`,
	 * * `'Green marker'`,
	 * * `'Pink marker'`,
	 * * `'Blue marker'`,
	 * * `'Red pen'`,
	 * * `'Green pen'`.
	 *
	 * @readonly
	 * @type {Object.<String,String>}
	 */
	get localizedOptionTitles() {
		const t = this.editor.t;

		return {
			'Yellow marker': t( 'Yellow marker' ),
			'Green marker': t( 'Green marker' ),
			'Pink marker': t( 'Pink marker' ),
			'Blue marker': t( 'Blue marker' ),
			'Red pen': t( 'Red pen' ),
			'Green pen': t( 'Green pen' )
		};
	}

	/**
	 * @inheritDoc
	 */
	static get pluginName() {
		return 'FontColorUI';
	}

	/**
	 * @inheritDoc
	 */
	init() {
		const options = __getUiOptions(this.editor);

		for ( const option of options ) {
			this._addHighlighterButton( option );
		}

		this._addDropdown( options );
	}



	/**
	 * Creates a toolbar button from the provided fontColor option.
	 *
	 * @param {module:fontColor/fontColor~HighlightOption} option
	 * @private
	 */
	_addHighlighterButton( option ) {
		const command = this.editor.commands.get( 'fontColor' );

		// TODO: change naming
		this._addButton( 'fontColor:' + option.model, option.title, getIconForType( option.type ), option.model, decorateHighlightButton );

		function decorateHighlightButton( button ) {
			button.bind( 'isEnabled' ).to( command, 'isEnabled' );
			button.bind( 'isOn' ).to( command, 'value', value => value === option.model );
			button.iconView.fillColor = option.color;
		}
	}

	/**
	 * Internal method for creating fontColor buttons.
	 *
	 * @param {String} name The name of the button.
	 * @param {String} label The label for the button.
	 * @param {String} icon The button icon.
	 * @param {Function} [decorateButton=()=>{}] Additional method for extending the button.
	 * @private
	 */
	_addButton( name, label, icon, value, decorateButton = () => {} ) {
		const editor = this.editor;

		editor.ui.componentFactory.add( name, locale => {
			const buttonView = new ButtonView( locale );

			const localized = this.localizedOptionTitles[ label ] ? this.localizedOptionTitles[ label ] : label;

			buttonView.set( {
				label: localized,
				icon,
				tooltip: true
			} );

			buttonView.on( 'execute', () => {
				editor.execute( 'fontColor', { value } );
				editor.editing.view.focus();
			} );

			// Add additional behavior for buttonView.
			decorateButton( buttonView );

			return buttonView;
		} );
	}

	/**
	 * Creates the split button dropdown UI from the provided fontColor options.
	 *
	 * @param {Array.<module:fontColor/fontColor~HighlightOption>} options
	 * @private
	 */
	_addDropdown( options ) {
		const editor = this.editor;
		const t = editor.t;
		const componentFactory = editor.ui.componentFactory;

		const startingHighlighter = options[ 0 ];

		const optionsMap = options.reduce( ( retVal, option ) => {
			retVal[ option.model ] = option;

			return retVal;
		}, {} );

		componentFactory.add( 'fontColor', locale => {
			const command = editor.commands.get( 'fontColor' );
			const dropdownView = createDropdown( locale, SplitButtonView );
			const splitButtonView = dropdownView.buttonView;

			splitButtonView.set( {
				tooltip: t( 'Text Color' ),
				// Holds last executed fontColorer.
				lastExecuted: startingHighlighter.model,
				// Holds current fontColorer to execute (might be different then last used).
				commandValue: startingHighlighter.model
			} );

			// Dropdown button changes to selection (command.value):
			// - If selection is in fontColor it get active fontColor appearance (icon, color) and is activated.
			// - Otherwise it gets appearance (icon, color) of last executed fontColor.
			splitButtonView.bind( 'icon' ).to( command, 'value', value => getIconForType( getActiveOption( value, 'type' ) ) );
			splitButtonView.bind( 'color' ).to( command, 'value', value => getActiveOption( value, 'color' ) );
			splitButtonView.bind( 'commandValue' ).to( command, 'value', value => getActiveOption( value, 'model' ) );
			splitButtonView.bind( 'isOn' ).to( command, 'value', value => !!value );

			splitButtonView.delegate( 'execute' ).to( dropdownView );

			__addRowBtnS(this.editor.config.get( 'fontColor.col1' ), componentFactory, dropdownView,this);

			__addRowBtnByCol(this.editor.config.get( 'fontColor.col2' ), componentFactory, dropdownView,this);

			__addRowBtnByCol(this.editor.config.get( 'fontColor.col3' ), componentFactory, dropdownView,this);

			__addRowBtnByCol(this.editor.config.get( 'fontColor.col4' ), componentFactory, dropdownView,this);

			__addRowBtnByCol(this.editor.config.get( 'fontColor.col5' ), componentFactory, dropdownView,this);



			bindToolbarIconStyleToActiveColor( dropdownView );

			// Execute current action from dropdown's split button action button.
			splitButtonView.on( 'execute', () => {
				editor.execute( 'fontColor', { value: splitButtonView.commandValue } );
				editor.editing.view.focus();
			} );

			// Returns active fontColorer option depending on current command value.
			// If current is not set or it is the same as last execute this method will return the option key (like icon or color)
			// of last executed fontColorer. Otherwise it will return option key for current one.
			function getActiveOption( current, key ) {
				const whichHighlighter = !current ||
				current === splitButtonView.lastExecuted ? splitButtonView.lastExecuted : current;

				return optionsMap[ whichHighlighter ][ key ];
			}

			return dropdownView;
		} );
	}
}

// Extends split button icon style to reflect last used button style.
function bindToolbarIconStyleToActiveColor( dropdownView ) {
	const actionView = dropdownView.buttonView.actionView;

	actionView.iconView.bind( 'fillColor' ).to( dropdownView.buttonView, 'color' );
}

// Returns icon for given fontColor type.
function getIconForType( type ) {
	return penIcon;
}
