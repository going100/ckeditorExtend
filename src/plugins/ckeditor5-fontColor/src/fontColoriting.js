/**
 * @license Copyright (c) 2003-2018, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md.
 */

/**
 * @module alignment/alignmentediting
 */
import Plugin from '@ckeditor/ckeditor5-core/src/plugin';
import FontColorCommand from "./fontColorCommand";

function __createCssStr(options) {
	//生成样式信息
	// var ary=[];
	// options.forEach(function (item, index) {
	// 	ary.push('.'+(item["class"].replace(/#/,"_"))+`{
	// 			color: ${item['color']};
	// 			background-color: transparent;
	// 		}`);
	// });
	// console.log(ary.join(""),"--------------------------")
}
//颜色定义
var __colorAry=[
	['#000000','#800000','#8B4513','#2F4F4F','#008080','#000080','#4B0082','#696969'],
	['#B22222','#A52A2A','#DAA520','#006400','#40E0D0','#0000CD','#800080','#808080'],
	['#FF0000','#FF8C00','#FFD700','#008000','#00FFFF','#0000FF','#EE82EE','#A9A9A9'],
	['#FFA07A','#FFA500','#FFFF00','#00FF00','#AFEEEE','#ADD8E6','#DDA0DD','#D3D3D3'],
	['#FFF0F5','#FAEBD7','#FFFFE0','#F0FFF0','#F0FFFF','#F0F8FF','#E6E6FA','#FFFFFF']
];
//将分组的数据合并成一个options
function __getOptions(editor) {
	var allAry=[];
	for(var colNum=1;colNum<=__colorAry.length;colNum++){
		var ary=editor.config.get( 'fontColor.col'+colNum);
		allAry.push(...ary);
	}
	return allAry;
}
//定义数据结构
function __getDefJson() {
	var defJson={};
	for(var colNum=1;colNum<=__colorAry.length;colNum++){
		var colElsAry=__colorAry[colNum-1];var colAry=[];
		for(var num=0;num<colElsAry.length;num++){
			colAry.push(
				{
					model: 'color'+colElsAry[num],
					class: 'marker'+(colElsAry[num]).replace(/#/,"_"),
					title: colElsAry[num],
					color: colElsAry[num],
					type: 'marker'
				}
			);
		}
		defJson['col'+colNum]=colAry;
	}
	return defJson;
}

const FONT_COLOR = 'fontColor';
export default class FontColoriting extends Plugin {

	/**
	 * @inheritDoc
	 */
	constructor( editor ) {
		super( editor );
		editor.config.define( 'fontColor', __getDefJson());
	}

	/**
	 * @inheritDoc
	 */
	init() {
		const editor = this.editor;

		// Allow highlight attribute on text nodes.
		editor.model.schema.extend( '$text', { allowAttributes: 'fontColor' } );
		editor.model.schema.extend( '$text', { allowAttributes: 'style' } );

		const options = __getOptions(editor);
		//创建样式字符串
		__createCssStr(options);

		// Set-up the two-way conversion.
		editor.conversion.attributeToElement( _buildDefinition( options ) );

		editor.commands.add( 'fontColor', new FontColorCommand( editor ) );
	}
}

// Converts the options array to a converter definition.
//
// @param {Array.<module:highlight/highlight~HighlightOption>} options An array with configured options.
// @returns {module:engine/conversion/conversion~ConverterDefinition}
function _buildDefinition( options ) {
	const definition = {
		model: {
			key: 'fontColor',
			values: []
		},
		view: {}
	};

	for ( const option of options ) {
		definition.model.values.push( option.model );
		definition.view[ option.model ] = {
			name: 'span',
			classes: option.class
		};
	}

	return definition;
}
