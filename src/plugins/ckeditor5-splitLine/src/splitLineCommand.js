

import Command from '@ckeditor/ckeditor5-core/src/command';
import first from '@ckeditor/ckeditor5-utils/src/first';

const SPLITLINE = 'splitLine';

export default class SplitLineCommand extends Command {

	/**
	 * Updates the command's {@link #value} and {@link #isEnabled} based on the current selection.
	 */
	refresh() {
		this.isEnabled = true;
	}

	execute() {
		const editor = this.editor;
		const model = editor.model;
		const document = model.document;
		const selection = document.selection;

		const docFrag = model.change( writer => {
			const ranges = model.schema.getValidRanges( selection.getRanges(), 'splitLine' );

			if ( selection.isCollapsed ) {
				writer.setSelectionAttribute( 'splitLine', 'bottomLine' );
			} else {
				for ( const range of ranges ) {
					if ( fontColorValue ) {
						writer.setAttribute( 'splitLine', 'bottomLine', range );
					}
				}
			}

			// var viewFragment = editor.data.processor.toView( "<a href='bottomLine'>-</a>");
			// var modelFragment = editor.data.toModel( viewFragment );
			// editor.model.insertContent( modelFragment, editor.model.document.selection );
			// //替换样式
			// setTimeout(function () {
			// 	$(".ck-content a[href='bottomLine']").each(function () {
			// 		$(this).parent().addClass("bottomLine").html("");
			// 	})
			// },1)

		} );
	}
}

