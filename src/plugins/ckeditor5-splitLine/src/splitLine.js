
import Plugin from '@ckeditor/ckeditor5-core/src/plugin';

import SplitLineEditing from './splitLineEditing';
import SplitLineUI from './splitLineUi';

export default class SplitLine extends Plugin {
	/**
	 * @inheritDoc
	 */
	static get requires() {
		return [ SplitLineEditing, SplitLineUI ];
	}

	/**
	 * @inheritDoc
	 */
	static get pluginName() {
		return 'splitLine';
	}
}
