

import Plugin from '@ckeditor/ckeditor5-core/src/plugin';
import ButtonView from '@ckeditor/ckeditor5-ui/src/button/buttonview';
import splitLineIcon from '../theme/icons/splitLine.svg';
const SPLITLINE = 'splitLine';


export default class SplitLineUI extends Plugin {
	/**
	 * @inheritDoc
	 */
	init() {
		const editor = this.editor;
		const t = editor.t;

		// Add bold button to feature components.
		editor.ui.componentFactory.add( SPLITLINE, locale => {
			const command = editor.commands.get( SPLITLINE );
			const view = new ButtonView( locale );

			view.set( {
				label: t( 'Insert Horizontal Line' ),
				icon: splitLineIcon,
				tooltip: true
			} );

			view.bind( 'isEnabled' ).to( command );
			// view.bind( 'isOn' ).to( command, 'value', value =>{return true;});
			// Execute command.
			this.listenTo( view, 'execute', ()=>{
				editor.execute( SPLITLINE );
				editor.editing.view.focus();
			} );

			return view;
		} );
	}
}


