/**
 * @license Copyright (c) 2003-2018, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md.
 */

/**
 * @module alignment/alignmentediting
 */
import Plugin from '@ckeditor/ckeditor5-core/src/plugin';
import SplitLineCommand from './splitLineCommand';
const SPLITLINE = 'splitLine';


export default class SplitLineEditing extends Plugin {
	/**
	 * @inheritDoc
	 */
	init() {
		const editor = this.editor;
		const schema = editor.model.schema;

		// editor.model.schema.extend( '$text', { allowAttributes: 'splitLine' } );
		// Create bold command.
		editor.conversion.attributeToElement( _buildDefinition( [{model:'bottomLine',class:'bottomLine'}] ) );

		editor.commands.add( SPLITLINE, new SplitLineCommand(editor) );
	}
}
function _buildDefinition( options ) {
	const definition = {
		model: {
			key: 'splitLine',
			values: []
		},
		view: {}
	};

	for ( const option of options ) {
		definition.model.values.push( option.model );
		definition.view[ option.model ] = {
			name: 'p',
			classes: option.class
		};
	}

	return definition;
}

