/**
 * @license Copyright (c) 2003-2018, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md.
 */

/**
 * @module alignment/alignmentediting
 */
import Plugin from '@ckeditor/ckeditor5-core/src/plugin';
import Indentcommand from './indentcommand';

const INDENT = 'indent';


export default class IndentEditing extends Plugin {
	/**
	 * @inheritDoc
	 */
	init() {
		const editor = this.editor;
		// Allow bold attribute on text nodes.
		editor.model.schema.extend( '$text', { allowAttributes: INDENT } );

		// Build converter from model to view for data and editing pipelines.
		var valueArys= _getValuesAry();

		editor.conversion.attributeToAttribute( {
			model: {
				key: INDENT,
				values: valueArys
			},
			view: _getViewConfigs(valueArys)
		} );
		// Create bold command.
		editor.commands.add( INDENT, new Indentcommand(editor) );

		// Set the Ctrl+B keystroke.
		editor.keystrokes.set( 'CTRL+I', INDENT );
	}
}

/**
 * 将20的倍数帅选出来
 * @returns {Array}
 * @private
 */
function _getValuesAry(){
	var valueArys=[];
	for(var num=1;num<=780;num++){
		if(num%20===0){
			valueArys.push(num+"");
		}
	}
	return valueArys;
}

function _getViewConfigs(valueArys) {
	var backJson={};
	valueArys.forEach(function (item, index) {
		backJson[item+""]={
			name: 'p',
			key: 'style',
			value: {
				'margin-left': item+'px',
			}
		}
	})
	return backJson;
}
