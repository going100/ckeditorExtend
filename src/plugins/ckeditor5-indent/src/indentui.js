

import Plugin from '@ckeditor/ckeditor5-core/src/plugin';
import ButtonView from '@ckeditor/ckeditor5-ui/src/button/buttonview';

import IndentIcon from '../theme/icons/indent.svg';



const INDENT = 'indent';

export default class IndentUI extends Plugin {
	/**
	 * @inheritDoc
	 */
	init() {
		const editor = this.editor;
		const t = editor.t;

		// Add bold button to feature components.
		editor.ui.componentFactory.add( INDENT, locale => {
			const command = editor.commands.get( INDENT );
			const view = new ButtonView( locale );

			view.set( {
				label: t( 'Increase Indent' ),
				icon: IndentIcon,
				keystroke: 'CTRL+I',
				tooltip: true
			} );

			view.bind( 'isEnabled' ).to( command );
			// view.bind( 'isOn' ).to( command, 'value', value =>{return true;});
			// Execute command.
			this.listenTo( view, 'execute', ()=>{
				editor.execute( INDENT );
				editor.editing.view.focus();
			} );

			return view;
		} );
	}
}

// Sets the alignment attribute on blocks.
// @private
function setIndentOnSelection( blocks, writer, alignment ) {
	for ( const block of blocks ) {
		writer.setAttribute( "margin", alignment, block );
	}
}

