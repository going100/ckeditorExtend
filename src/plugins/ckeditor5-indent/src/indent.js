
import Plugin from '@ckeditor/ckeditor5-core/src/plugin';

import IndentEditing from './indentediting';
import IndentUI from './indentui';

export default class Indent extends Plugin {
	/**
	 * @inheritDoc
	 */
	static get requires() {
		return [ IndentEditing, IndentUI ];
	}

	/**
	 * @inheritDoc
	 */
	static get pluginName() {
		return 'indent';
	}
}
