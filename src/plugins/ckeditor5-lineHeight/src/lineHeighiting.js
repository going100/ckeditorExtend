/**
 * @license Copyright (c) 2003-2018, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md.
 */

/**
 * @module alignment/alignmentediting
 */
import Plugin from '@ckeditor/ckeditor5-core/src/plugin';
import LineHeightcommand from './lineHeightcommand';

const LINE_HEIGHT = 'lineHeight';

export default class LineHeighiting extends Plugin {

	/**
	 * @inheritDoc
	 */
	constructor( editor ) {
		super( editor );

		editor.config.define( 'lineHeight', {
			options: [
				'normal',
				'1',
				'1.5',
				'2',
				'2.5',
				'3'
			]
		} );
	}
	/**
	 * @inheritDoc
	 */
	init() {
		const editor = this.editor;

		// Allow fontFamily attribute on text nodes.
		editor.model.schema.extend( '$text', { allowAttributes: LINE_HEIGHT } );


		// Set-up the two-way conversion.
		editor.conversion.attributeToElement( {
			model: {
				name:'$text',
				key: LINE_HEIGHT,
				values: ['normal','1','1.5','2','2.5','3']
			},
			view:{
				normal: {
					name: 'span',
					styles: {
						'line-height': '1.5'
					}
				},
				'1': {
					name: 'span',
					styles: {
						'line-height': '1'
					}
				},
				'1.5': {
					name: 'span',
					styles: {
						'line-height': '1.5'
					}
				},
				'2': {
					name: 'span',
					styles: {
						'line-height': '2'
					}
				},
				'2.5': {
					name: 'span',
					styles: {
						'line-height': '2.5'
					}
				},
				'3': {
					name: 'span',
					styles: {
						'line-height': '3'
					}
				},
			}
		}  );

		editor.commands.add( LINE_HEIGHT, new LineHeightcommand( editor,LINE_HEIGHT ) );
	}
}
