
import Plugin from '@ckeditor/ckeditor5-core/src/plugin';

import LineHeighiting from './lineHeighiting';
import LineHeightUi from './lineHeightUi';

/**
 * 行距插件
 */
export default class Indent extends Plugin {
	/**
	 * @inheritDoc
	 */
	static get requires() {
		return [ LineHeighiting, LineHeightUi ];
	}

	/**
	 * @inheritDoc
	 */
	static get pluginName() {
		return 'lineHeight';
	}
}
