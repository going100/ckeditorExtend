function __getOptionDefinition( option ) {
	// Treat any object as full item definition provided by user in configuration.
	if ( typeof option === 'object' ) {
		return option;
	}
	if ( typeof option !== 'string' ) {
		return;
	}
	return {
		title: option,
		model: option
	};
}

function getLineHeightOptions( configuredOptions ) {
	return configuredOptions
		.map( __getOptionDefinition )
		.filter( option => !!option );
}
/**
 * 获取下拉值
 * @param editor
 */
export function getLocalizedOptions( editor ) {
	const t = editor.t;
    //需要对下拉的值进行一次修饰
	const options = getLineHeightOptions( editor.config.get( 'lineHeight.options' ) );
	return options;
}


