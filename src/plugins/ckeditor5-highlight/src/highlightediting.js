/**
 * @license Copyright (c) 2003-2018, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md.
 */

/**
 * @module highlight/highlightediting
 */

import Plugin from '@ckeditor/ckeditor5-core/src/plugin';

import HighlightCommand from './highlightcommand';

/**
 * The highlight editing feature. It introduces the {@link module:highlight/highlightcommand~HighlightCommand command} and the `highlight`
 * attribute in the {@link module:engine/model/model~Model model} which renders in the {@link module:engine/view/view view}
 * as a `<mark>` element with a `class` attribute (`<mark class="marker-green">...</mark>`) depending
 * on the {@link module:highlight/highlight~HighlightConfig configuration}.
 *
 * @extends module:core/plugin~Plugin
 */
//颜色定义
var __bgColorAry=[
	['#000000','#800000','#8B4513','#2F4F4F','#008080','#000080','#4B0082','#696969'],
	['#B22222','#A52A2A','#DAA520','#006400','#40E0D0','#0000CD','#800080','#808080'],
	['#FF0000','#FF8C00','#FFD700','#008000','#00FFFF','#0000FF','#EE82EE','#A9A9A9'],
	['#FFA07A','#FFA500','#FFFF00','#00FF00','#AFEEEE','#ADD8E6','#DDA0DD','#D3D3D3'],
	['#FFF0F5','#FAEBD7','#FFFFE0','#F0FFF0','#F0FFFF','#F0F8FF','#E6E6FA']
];

function __createBgCssStr(options) {
	// 生成样式信息
	var ary=[];
	options.forEach(function (item, index) {
		ary.push('.'+(item["class"].replace(/#/,"_"))+`{ 
				background-color: ${item['color']};
			}`);
	});
}
//将分组的数据合并成一个options
function __getBgOptions(editor) {
	var allAry=[];
	for(var colNum=1;colNum<=__bgColorAry.length;colNum++){
		var ary=editor.config.get( 'highlight.col'+colNum);
		allAry.push(...ary);
	}
	return allAry;
}
//定义数据结构
function __getBgDefJson() {
	var defJson={};
	for(var colNum=1;colNum<=__bgColorAry.length;colNum++){
		var colElsAry=__bgColorAry[colNum-1];var colAry=[];
		for(var num=0;num<colElsAry.length;num++){
			colAry.push(
				{
					model: 'color'+colElsAry[num],
					class: 'markerBg'+(colElsAry[num]).replace(/#/,"_"),
					title: colElsAry[num],
					color: colElsAry[num],
					type: 'marker'
				}
			);
		}
		defJson['col'+colNum]=colAry;
	}
	return defJson;
}

export default class HighlightEditing extends Plugin {
	/**
	 * @inheritDoc
	 */
	constructor( editor ) {
		super( editor );

		editor.config.define( 'highlight', __getBgDefJson());
	}

	/**
	 * @inheritDoc
	 */
	init() {
		const editor = this.editor;

		// Allow highlight attribute on text nodes.
		editor.model.schema.extend( '$text', { allowAttributes: 'highlight' } );

		const options = __getBgOptions(editor);
		//创建样式字符串
		__createBgCssStr(options);
		// Set-up the two-way conversion.
		editor.conversion.attributeToElement( _buildDefinition( options ) );

		editor.commands.add( 'highlight', new HighlightCommand( editor ) );
	}
}

// Converts the options array to a converter definition.
//
// @param {Array.<module:highlight/highlight~HighlightOption>} options An array with configured options.
// @returns {module:engine/conversion/conversion~ConverterDefinition}
function _buildDefinition( options ) {
	const definition = {
		model: {
			key: 'highlight',
			values: []
		},
		view: {}
	};

	for ( const option of options ) {
		definition.model.values.push( option.model );
		definition.view[ option.model ] = {
			name: 'mark',
			classes: option.class
		};
	}

	return definition;
}
