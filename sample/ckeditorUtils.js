function addCkEditor5Attachment() {
    alert(123);
}

function UploadAdapter(_loader, url) {
    this.loader = _loader;
    this.url = url;
}

UploadAdapter.prototype={
    upload: function () {
        var loadObj=this;
        var maxSize = "1024";
        return new Promise(function (resolve, reject) {
            var data = new FormData();
            data.append('upload', loadObj.loader.file);
            data.append('allowSize', maxSize);//允许图片上传的大小/兆
            $.ajax({
                url: loadObj.url,
                type: 'POST',
                data: data,
                dataType: 'html',
                processData: false,
                contentType: false,
                success: function (data) {
                    eval('var backData='+ data);
                    if (backData.code===1||backData.code==='1') {
                        resolve({
                            default: backData.url
                        });
                    } else {
                        reject(backData.msg);
                    }

                }
            });
        })
    },
    abort:function () {

    }
}

var  ckeditor5Utils={
    instances:{},
    //获取编辑的内容
    getEditorContent:function (editorId,$inputObj) {
        var editorObj = ckeditor5Utils.instances[editorId];
        if (editorObj != null) {
            var tc = $inputObj.attr('comp');
            if (tc) {
                var tj = $.parseJSON('{' + tc + '}')
                if (tj.type === 'editor' && tj.contentType === 'html') {
                    var data = editorObj.getData();
                    // 替换&#8203和\t\n
                    return data.replace(/\u200B/g,'').replace(/\n/g,'').replace(/\t/g,'');
                }
            }
        }else{
            var retstr= $("#"+editorId).val();
            if(retstr!=null){
                retstr=retstr.replace(/\n/g, "<br/>");
            }
            return retstr;
        }
        return null;
    },
    //初始化带边框的editor
    initClassicEditor:function(editorObjEl,settings,contextPath){
        var editorId=editorObjEl.id;
        ClassicEditor
            .create(document.querySelector('#' + editorId), {
                    //editor加载中文简体，默认是英文
                    language: 'zh-cn',
                    ckfinder: {
                        uploadUrl: contextPath
                        + '/fileUpload.do?method=processUpload&type=1&applicationCategory='
                        + settings.category + '&extensions=jpg,gif,jpeg,png&maxSize='
                        + settings.maxSize
                    }
                }
            ).then(function (editor) {
            //如果对象存在 则先销毁以前保存的对象信息
            if (ckeditor5Utils.instances[editorId]) {
                ckeditor5Utils.instances[editorId].destroy();
            }
            //将对象缓存起来
            ckeditor5Utils.instances[editorId] = editor;
            //设置正文区域的样式信息
            ckeditor5Utils.initEditorStyle(editorObjEl,".ck-editor__main",786);
        }).catch(function (error) {
            console.error(error);
        });
    },
    //修改编辑器的样式信息
    initEditorStyle:function(editorObjEl,editorDivId,width){
        // 18310962871
        var ckeContentHeight = $(editorObjEl).closest(".content_view").height()-40;
        $(editorDivId+" .ck-content").css("height","100%");
        //设置高度
        document.querySelector(editorDivId).style.height= ckeContentHeight+"px";
        //设置宽度
        document.querySelector(editorDivId).style.width= width+"px";
        document.querySelector(editorDivId).style.margin='auto';
        document.querySelector(editorDivId).style.padding= "15px 30px";
        document.querySelector(editorDivId).style.backgroundColor='#FFF';
        document.querySelector(editorDivId).style.boxSizing="border-box";
        $(editorObjEl).closest(".content_view").css("background-color",'rgb(216, 217, 219)');

    },
    //插入文本内容
    initEditorContent:function (editorId,content) {
        var editor=ckeditor5Utils.instances[editorId];
        var viewFragment = editor.data.processor.toView( content );
        var modelFragment = editor.data.toModel( viewFragment );
        editor.model.insertContent( modelFragment, editor.model.document.selection );
    },
    //初始化文档类型的编辑器
    initDocEditor:function(editorObjEl,settings,contextPath){
        var maxSize = "10240000";
        var uploadUrl= contextPath
            + '/fileUpload.do?method=processUpload&type=1&applicationCategory=1&extensions=jpg,gif,jpeg,png&maxSize='+maxSize +
            '&callback=addCkEditor5Attachment&closeWindow=true';

        var editorId=editorObjEl.id;
        $('#'+editorId).before("<div id='"+editorId+"-editor-bar'></div><div id='"+editorId+"-div'></div>");
        DecoupledEditor.create(document.querySelector('#'+editorId+"-div"),{
                language: 'zh-cn',
            })
            .then(function (editor) {
                // 加载了适配器
                editor.plugins.get( 'FileRepository' ).createUploadAdapter = function( loader ) {
                    return new UploadAdapter( loader,uploadUrl);
                };

                //如果对象存在 则先销毁以前保存的对象信息
                if (ckeditor5Utils.instances[editorId]) {
                    ckeditor5Utils.instances[editorId].destroy();
                }
                //将对象缓存起来
                ckeditor5Utils.instances[editorId] = editor;
                //添加工具栏相关信息
                var toolbarContainer = document.querySelector('#'+editorId+"-editor-bar");
                toolbarContainer.appendChild(editor.ui.view.toolbar.element);
                //设置正文区域的样式信息
                ckeditor5Utils.initEditorStyle(editorObjEl,'#'+editorId+"-div",786);
                //    设置正文内容 18310962871  1635375660
                var content=$("#"+editorId).text();
                ckeditor5Utils.initEditorContent(editorId,content);
            })
            .catch(function (error) {
                console.error(error);
            });


    }
}