/**
 * @license Copyright (c) 2003-2018, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see https://ckeditor.com/legal/ckeditor-oss-license
 */
function getContextPath(){
    return '/seeyon';
}

function __initFCKLang(fontFamily) {
    var FCKLang = [];
    FCKLang['zh_CN'] = {
        /////字号
        fontSize_1: "7号",
        fontSize_2: "6号",
        fontSize_3: "5号",
        fontSize_4: "4号",
        fontSize_5: "3号",
        fontSize_6: "2号",
        fontSize_7: "1号",

        /////字体,以半角分号分隔，并以它结束
        //fontFamily    : "仿宋,仿宋_gb2312;宋体;黑体;隶书;华文行楷;楷体,楷体_gb2312;微软雅黑;幼圆;方正舒体;方正姚体;"
        fontFamily: fontFamily
    };

    FCKLang['zh-cn'] = FCKLang['zh_CN'];
    FCKLang['zh_TW'] = {
        /////字號
        fontSize_1: "7號",
        fontSize_2: "6號",
        fontSize_3: "5號",
        fontSize_4: "4號",
        fontSize_5: "3號",
        fontSize_6: "2號",
        fontSize_7: "1號",

        /////字體,以半角分號分隔，并以它結束
        fontFamily: "宋體;黑體;隸書;華文行楷;"
    };
    FCKLang['zh-tw'] = FCKLang['zh_TW'];
    FCKLang['en'] = {
        /////Font size
        fontSize_1: "7",
        fontSize_2: "6",
        fontSize_3: "5",
        fontSize_4: "4",
        fontSize_5: "3",
        fontSize_6: "2",
        fontSize_7: "1",

        /////Font, separated by and finished with semicolon of half angle
        fontFamily: ""
    };
    return FCKLang;
}
//获取当前系统的语言
function __getCurSysLang(){
    return 'zh';
}
CKEDITOR.editorConfig = function( config ) {
    var fontNames = null;

    var fontFamily ="仿宋,仿宋_gb2312;宋体;黑体;隶书;华文行楷;楷体,楷体_gb2312;微软雅黑;幼圆;方正舒体;方正姚体;"
    if(null != fontNames && undefined != fontNames){
        fontFamily=fontFamily+fontNames;
    }
    //初始化编辑器的语音信息
    var FCKLang = __initFCKLang(fontFamily);
    //获取当前系统的语言
    var lang=__getCurSysLang();

    //根据语言获取对应的相关字体
    function getLang(key){
        if(!lang){
            lang = this._locale;
        }
        if(undefined == FCKLang[lang]){
            return '';
        }else{
            return  FCKLang[lang][key] || '';
        }
    }

    var baseUrl = getContextPath() + '/common/ckeditor/';
    //是否使用完整的html编辑模式如使用，其源码将包含：<html><body></body></html>等标签
    config.fullPage = false;
    config.font_defaultLabel = 'Arial';
    config.fontSize_defaultLabel = '16px';
    config.lineheight_defaultLabel = '1.5';

    config.editingBlock = true;
    config.height = '100%';
    config.contentsCss = [baseUrl+'../skin/dist/common/skin.css',baseUrl+'../../skin/dist/common/skin.css',baseUrl+'../../common/all-min.css',baseUrl+'contents.css'];
    config.coreStyles_italic =
        {
            element : 'em',
            attributes : { 'style' : 'font-style:italic' }
        };
    // Define changes to default configuration here. For example:
    config.language = lang.toLowerCase().replace('_','-');
    // config.uiColor = '#AADC6E';
    config.toolbar = 'Basic';
    config.removePlugins = 'elementspath,magicline,form';  // ,pastefromword
    config.forcePasteAsPlainText = false; //是否强制复制来的内容去除格式plugins/pastetext/plugin.js
    config.toolbar_Full =
        [
            { name: 'document', items : [ 'Source','-','Save','NewPage','DocProps','Preview','Print','-','Templates' ] },
            { name: 'clipboard', items : [ 'Cut','Copy','Paste','PasteText','PasteFromWord','-','Undo','Redo' ] },
            { name: 'editing', items : [ 'Find','Replace','-','SelectAll','-','SpellChecker', 'Scayt' ] },
            { name: 'forms', items : [ 'Form', 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select', 'Button', 'ImageButton',
                    'HiddenField' ] },
            '/',
            { name: 'basicstyles', items : [ 'Bold','Italic','Underline','Strike','Subscript','Superscript','-','RemoveFormat' ] },
            { name: 'paragraph', items : [ 'NumberedList','BulletedList','-','Outdent','Indent','-','Blockquote','CreateDiv',
                    '-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','-','BidiLtr','BidiRtl' ] },
            { name: 'links', items : [ 'Link','Unlink','Anchor' ] },
            { name: 'insert', items : [ 'Image','Flash','Table','HorizontalRule','Smiley','SpecialChar','PageBreak','Iframe' ] },
            '/',
            { name: 'styles', items : [ 'Styles','Format','Font','FontSize','lineheight' ] },
            { name: 'colors', items : [ 'TextColor','BGColor' ] },
            { name: 'tools', items : [ 'Maximize', 'ShowBlocks','-','About' ] }
        ];

    config.toolbar_Default = [
        ['FontFormat','FontName','FontSize'],
        ['Cut','Copy','Paste','PasteText'],
        ['Undo','Redo','-','Find','Replace','-','RemoveFormat'],
        ['Bold','Italic','Underline','StrikeThrough','-','Subscript','Superscript'],
        ['TextColor','BGColor'],
        ['NumberedList','BulletedList','-','Outdent','Indent'],
        ['JustifyLeft','JustifyCenter','JustifyRight'],
        ['Link','Unlink','Anchor'],
        ['Image','Flash','ctpassociate','Table','Rule','SpecialChar','PageBreak']
    ] ;

    config.toolbar_Basic = [
        { name: 'styles', items : [ 'Font','FontSize','lineheight','-' ] },
        ['Bold','Italic','Underline'],
        ['JustifyLeft','JustifyCenter','JustifyRight','-'],
        ['TextColor','BGColor'],
        ['NumberedList','BulletedList','Outdent','Indent','-'],
        //'/',
        ['Image','Flash','ctpassociate','Table','HorizontalRule','Link','Unlink'],
        ['Undo','Redo']
        //,['Save']
        //['Preview']
    ] ;
    config.toolbar_BasicAdmin = [
        ['Font','FontSize','-'],
        ['Bold','Italic','Underline'],
        ['JustifyLeft','JustifyCenter','JustifyRight','-'],
        ['TextColor','BGColor'],
        ['NumberedList','BulletedList','Outdent','Indent','-'],
        //'/',
        ['Image','Flash','Table','HorizontalRule','Link','Unlink'],
        ['Undo','Redo']
        //,['Save']
        //['Preview']
    ] ;
    config.toolbar_Mail = [
        ['Font','FontSize','-'],
        ['Bold','Italic','Underline'],
        ['JustifyLeft','JustifyCenter','JustifyRight','-'],
        ['TextColor','BGColor'],
        ['NumberedList','BulletedList','Outdent','Indent','-'],
        //'/',
        ['Image','Smiley','Flash','Table','HorizontalRule','Link','Unlink'],
        ['Undo','Redo']
    ] ;
    config.toolbar_Bbs = [
        ['Font','FontSize','-'],
        ['Bold','Italic','Underline'],
        ['JustifyLeft','JustifyCenter','JustifyRight','-'],
        ['TextColor','BGColor'],
        ['NumberedList','BulletedList','Outdent','Indent','-'],
        ['Image','Smiley','Flash','Table','Link','Unlink'],
        ['Undo','Redo']
    ] ;

    config.toolbar_BbsSimple = [
        ['Font','FontSize','-'],
        ['Bold','Italic','Underline'],
        ['JustifyLeft','JustifyCenter','JustifyRight','-'],
        ['TextColor','BGColor'],
        ['Image','Smiley','Flash','Link','Unlink'],
        ['Undo','Redo']
    ] ;

    config.toolbar_Simple = [
        ['Font','FontSize','-'],
        ['Bold','Italic','Underline'],
        ['JustifyLeft','JustifyCenter','JustifyRight','-'],
        ['TextColor','BGColor']
    ] ;

    config.toolbar_VerySimple = [
        ['Bold','Italic','Underline','TextColor','NumberedList','BulletedList']
    ] ;

    config.toolbar_Notice = [
        { name: 'styles', items : [ 'Font','FontSize','lineheight','-' ] },
        ['Bold','Italic','Underline'],
        ['JustifyLeft','JustifyCenter','JustifyRight','-'],
        ['TextColor','BGColor'],
        ['NumberedList','BulletedList','Outdent','Indent','-'],
        ['Image','Flash','Table','HorizontalRule','Link','Unlink'],
        ['Undo','Redo']
    ] ;

    config.skin = 'moono';


};


