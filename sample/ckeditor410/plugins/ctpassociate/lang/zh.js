CKEDITOR.plugins.setLang('ctpassociate', 'zh', {
  pluginname : '插入關聯',
  setting : '設置',
  title : '插入關聯',
  link : '關聯鏈接',
  linkIsMissing : '關聯鏈接不能為空',
  linktype : '關聯類型',
  linktype1 : '關聯文檔',
  linktype2 : '人員卡片',
  linktype3 : '上傳附件',
  openmode : '彈出模式',
  openmode1 : '在新窗口中打開',
  openmode2 : '在原位置打開',
  seperator : '、'
});
