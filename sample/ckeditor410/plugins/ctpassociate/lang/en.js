CKEDITOR.plugins.setLang('ctpassociate', 'en', {
  pluginname : 'Insert Associate',
  setting : 'Setting',
  title : 'Insert associate',
  link : 'Link',
  linkIsMissing : 'link is required',
  linktype : 'Link type',
  linktype1 : 'Associate document',
  linktype2 : 'People card',
  linktype3 : 'Upload attachement',
  openmode : 'Mode',
  openmode1 : 'New window',
  openmode2 : 'Same window',
  seperator : ','
});
